import React from 'react';
import { View, Text, TouchableOpacity, Image, StyleSheet } from 'react-native';
import { BeerColorIndicator } from './common';
import { BLACK, WHITE } from '../constants/Colors';
import { Beer } from '../types';

interface Props {
  index: number;
  beerData: Beer;
  action: () => void;
}

const BeerCard = ({ index, beerData, action }: Props) => {
  return (
    <TouchableOpacity
      key={index}
      onPress={action}
      style={styles.beerCardContainer}>
      <BeerColorIndicator color={beerData.color} size={50} />
      <Image
        source={beerData.image}
        resizeMode="contain"
        style={styles.beerBottleImageContainer}
      />
      <View>
        <Text style={styles.beerName}>{beerData.name}</Text>
        <Text style={styles.beerStyle}>{beerData.style}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  beerCardContainer: {
    justifyContent: 'space-between',
    width: 150,
    padding: 10,
    borderRadius: 20,
    shadowColor: BLACK,
    shadowRadius: 10,
    backgroundColor: WHITE,
    shadowOpacity: 0.3,
    marginRight: 20,
  },
  beerBottleImageContainer: {
    height: 120,
    width: 60,
    alignSelf: 'flex-end',
    marginTop: -50,
    position: 'absolute',
  },
  beerName: {
    fontWeight: '500',
    fontSize: 18,
    marginBottom: 5,
  },
  beerStyle: {
    color: 'grey',
  },
});

export default BeerCard;
