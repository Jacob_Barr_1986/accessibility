import React from 'react';
import { View, Text, StyleSheet, Image, ScrollView } from 'react-native';
import { Beer } from '../types';
import { OvalText } from './common/OvalText';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../constants/Constants';
import { RatingBar } from './common';
import LinearGradient from 'react-native-linear-gradient';
import { WHITE } from '../constants/Colors';

interface Props {
  selectedBeerInfo?: Beer;
}

const BeerPage = ({ selectedBeerInfo }: Props) => {
  if (!selectedBeerInfo) {
    return null;
  }

  const renderTopRow = () => {
    return (
      <View style={styles.topRow}>
        <OvalText text={`Alc. ${selectedBeerInfo.alcohol}%`} />
        <OvalText text={`${selectedBeerInfo.bottleCapacityML}ml`} />
      </View>
    );
  };

  return (
    <ScrollView>
      <LinearGradient
        colors={[selectedBeerInfo.color, WHITE]}
        style={styles.container}
        start={{ x: 0, y: 0 }}
        end={{ x: 0.5, y: 1 }}>
        {renderTopRow()}

        <Image
          resizeMode="contain"
          source={selectedBeerInfo.image}
          style={styles.bottleImage}
        />

        <View style={styles.descriptionContainer}>
          <Text style={styles.beerTitle}>{selectedBeerInfo.name}</Text>

          <RatingBar
            rating={selectedBeerInfo.rating}
            customStyle={{ marginVertical: 20 }}
          />

          <Text style={styles.beerDescription}>
            {selectedBeerInfo.description}
          </Text>
        </View>
      </LinearGradient>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  topRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  bottleImage: {
    height: SCREEN_HEIGHT * 0.5,
    width: SCREEN_WIDTH * 0.5,
    alignSelf: 'center',
    marginTop: -30,
  },
  descriptionContainer: {
    paddingTop: 20,
  },
  beerTitle: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  beerDescription: {
    fontSize: 20,
  },
});

export default BeerPage;
