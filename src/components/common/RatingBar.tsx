import React from 'react';
import { Image, StyleSheet, View, ViewStyle } from 'react-native';
import { Assets } from '../../assets/Assets';

interface Props {
  rating: number; // accepts values in the range of 0-10
  customStyle?: ViewStyle;
}

const renderStar = (isFilled: boolean, isHalf: boolean, index?: number) => {
  const ICON_SIZE = 32;
  let icon = Assets.FULL_STAR_UNFILLED;
  if (isFilled) {
    icon = isHalf ? Assets.HALF_STAR_FILLED : Assets.FULL_STAR_FILLED;
  } else {
    icon = isHalf ? Assets.HALF_STAR_UNFILLED : Assets.FULL_STAR_UNFILLED;
  }

  return (
    <Image
      key={index}
      style={{
        width: ICON_SIZE,
        height: ICON_SIZE,
      }}
      source={icon}
    />
  );
};

const renderFullStars = (numOfStars: number) => {
  const stars = [];
  for (let i = 0; i < numOfStars; i++) {
    stars.push(renderStar(true, false, i));
  }
  return stars;
};

const RatingBar = ({ rating, customStyle }: Props) => {
  const roundedRating = Math.round(rating);
  const numOfFullStars = Math.floor(roundedRating / 2);
  const includeHalfStar = roundedRating % 2;

  return (
    <View style={[styles.container, customStyle]}>
      {renderFullStars(numOfFullStars)}
      {!!includeHalfStar && renderStar(true, true)}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
});

export { RatingBar };
