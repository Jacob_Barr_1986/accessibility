import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { BAR_WIDTH } from '../../constants/Constants';
import { LIGHT_GREY } from '../../constants/Colors';
import { GestureEvent, PanGestureHandler } from 'react-native-gesture-handler';
import Animated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedStyle,
  useSharedValue,
} from 'react-native-reanimated';

interface Props {
  scalesArr: number[];
  action: (pickedValue: number) => void;
}

const DraggablePickerBar = ({ scalesArr, action }: Props) => {
  const x1 = useSharedValue(0);
  const pressed = useSharedValue(false);

  const eventHandler1 = useAnimatedGestureHandler<
    GestureEvent<Record<string, any>>,
    { startX: number }
  >({
    onStart: (event, ctx) => {
      pressed.value = true;
      ctx.startX = x1.value;
    },
    onActive: (event, ctx) => {
      if (
        ctx.startX + event.translationX > 0 &&
        ctx.startX + event.translationX < BAR_WIDTH - 20
      ) {
        x1.value = ctx.startX + event.translationX;
      }
    },
    onEnd: (event, ctx) => {
      pressed.value = false;
      const calculatedScaledValue =
        (x1.value / BAR_WIDTH) * scalesArr[scalesArr.length - 1];
      const roundValue = Math.round(calculatedScaledValue);

      runOnJS(action)(roundValue);
    },
  });

  const uas1 = useAnimatedStyle(() => {
    return {
      transform: [{ translateX: x1.value }],
    };
  });

  return (
    <View style={{ height: 40 }}>
      <View
        style={{
          width: BAR_WIDTH,
          height: 20,
          backgroundColor: LIGHT_GREY,
          borderWidth: 1,
        }}>
        <PanGestureHandler onGestureEvent={eventHandler1}>
          <Animated.View style={[styles.picker, uas1]} />
        </PanGestureHandler>

        <View style={styles.scaleRow}>
          {scalesArr.map((scale, index) => (
            <Text key={index}>{scale}</Text>
          ))}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  picker: {
    width: 20,
    height: 20,
    zIndex: 1,
    backgroundColor: 'red',
    borderWidth: 1,
  },
  scaleRow: {
    position: 'absolute',
    width: BAR_WIDTH,
    height: 20,
    top: 20,
    zIndex: 1000,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export { DraggablePickerBar };
