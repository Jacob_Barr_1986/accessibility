import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { DROPBOX_ROW_HEIGHT } from '../../constants/Constants';
import { LIGHT_GREY } from '../../constants/Colors';

interface Props {
  title: string;
  optionsArr: number[]; // numeric only at this point
  action: (selectedMinRating: number) => void;
}

const Dropbox = ({ title, optionsArr, action }: Props) => {
  const [dropboxExpanded, setDropboxExpanded] = useState<boolean>(false);

  const renderDropboxItems = () => {
    return optionsArr.map((rating, index) => {
      return (
        <TouchableOpacity
          style={styles.dropboxRowStyle}
          key={index}
          onPress={() => {
            action(rating);
            setDropboxExpanded(false);
          }}>
          <Text style={{ textAlign: 'center' }}>{rating}</Text>
        </TouchableOpacity>
      );
    });
  };

  return (
    <View
      style={{
        alignSelf: 'stretch',
        alignItems: 'center',
        borderWidth: 1,
        overflow: 'hidden',
      }}>
      <TouchableOpacity
        style={{ height: DROPBOX_ROW_HEIGHT }}
        onPress={() => {
          setDropboxExpanded(!dropboxExpanded);
        }}>
        <Text>{title}</Text>
      </TouchableOpacity>

      <View
        style={{
          height: dropboxExpanded ? optionsArr.length * DROPBOX_ROW_HEIGHT : 0,
          alignSelf: 'stretch',
        }}>
        {renderDropboxItems()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  dropboxRowStyle: {
    height: DROPBOX_ROW_HEIGHT,
    backgroundColor: LIGHT_GREY,
    borderWidth: 1,
    justifyContent: 'center',
  },
});

export { Dropbox };
