import React, { useState, useEffect, useRef } from 'react';
import {
  NativeScrollEvent,
  NativeSyntheticEvent,
  ScrollView,
  StyleSheet,
  View,
  ViewStyle,
} from 'react-native';
import { SCREEN_HEIGHT, SCREEN_WIDTH } from '../../constants/Constants';
import { CarouselCard } from './CarouselCard';
import { CarouselCardInterface } from '../../types';

interface Props {
  cards: CarouselCardInterface[];
  cardWidth?: number;
  height?: number;
  paddingHorizontal?: number;
  onCardIndexChange?: (index: number) => void;
  currentIndex?: number;
  customStyle?: ViewStyle;
}

const Carousel = ({
  cards,
  cardWidth = SCREEN_WIDTH,
  height = SCREEN_HEIGHT * 0.25,
  paddingHorizontal,
  onCardIndexChange,
  currentIndex,
  customStyle,
}: Props) => {
  const scrollViewRef = useRef<ScrollView>(null);
  let currentCardIndex = 0;

  useEffect(() => {
    if (
      currentIndex &&
      currentIndex >= 0 &&
      currentIndex < cards.length &&
      currentIndex !== currentCardIndex
    ) {
      scrollViewRef?.current?.scrollTo({
        x: currentIndex * cardWidth,
        animated: true,
      });
    }
  }, [currentIndex]);

  return (
    <View style={[{ height }, customStyle]}>
      <ScrollView
        ref={scrollViewRef}
        bounces={false}
        disableIntervalMomentum
        horizontal
        snapToAlignment="start"
        snapToInterval={cardWidth}
        decelerationRate="fast"
        showsHorizontalScrollIndicator={false}
        disableScrollViewPanResponder
        contentContainerStyle={[
          styles.scrollViewContentContainerStyle,
          { height },
        ]}
        onMomentumScrollEnd={(
          event: NativeSyntheticEvent<NativeScrollEvent>,
        ) => {
          const newCardIndex = Math.round(
            event.nativeEvent.contentOffset.x / cardWidth,
          );
          if (newCardIndex !== currentCardIndex) {
            currentCardIndex = newCardIndex;
            onCardIndexChange && onCardIndexChange(currentCardIndex);
          }
        }}>
        {cards.map((card, index) => {
          const {
            title,
            isOpen,
            hours,
            distance,
            backgroundColor,
            isCurbsidePickupAvailable,
          } = card;
          return (
            <CarouselCard
              key={index}
              width={cardWidth}
              backgroundColor={backgroundColor}
              isLast={index === cards.length - 1}
              paddingHorizontal={paddingHorizontal}
              title={title}
              isOpen={isOpen}
              hours={hours}
              distance={distance}
              isCurbsidePickupAvailable={isCurbsidePickupAvailable}
              isSelected={index === currentIndex}
            />
          );
        })}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollViewContentContainerStyle: {
    backgroundColor: 'transparent',
  },
});
export { Carousel };
