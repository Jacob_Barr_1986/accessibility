import React from 'react';
import { View } from 'react-native';

interface Props {
  size: number;
  color: string;
}

const BeerColorIndicator = ({ size, color }: Props) => {
  return (
    <View
      style={{
        width: size,
        height: size,
        borderRadius: size * 0.5,
        backgroundColor: color,
      }}
    />
  );
};

export { BeerColorIndicator };
