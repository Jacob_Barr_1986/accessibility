import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { LIGHT_GREY } from '../../constants/Colors';

interface Props {
  text: string;
}

const OvalText = ({ text }: Props) => {
  return (
    <View style={styles.container}>
      <Text>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 20,
    backgroundColor: LIGHT_GREY,
    paddingVertical: 5,
    paddingHorizontal: 20,
  },
});

export { OvalText };
