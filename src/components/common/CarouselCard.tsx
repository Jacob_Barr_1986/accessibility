import React, { useState, useEffect } from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { CarouselCardInterface } from '../../types';
import { SCREEN_WIDTH } from '../../constants/Constants';
import {
  CARD_SHADOW_COLOR_LIGHT_GREY,
  TEXT_GREEN,
  TEXT_GREY,
  TEXT_RED,
} from '../../constants/Colors';

interface Props extends CarouselCardInterface {
  backgroundColor?: string;
  paddingHorizontal?: number;
  width?: number;
  isLast?: boolean;
  isSelected?: boolean;
}

const CarouselCard = ({
  backgroundColor = 'white',
  width = SCREEN_WIDTH,
  isLast,
  paddingHorizontal = 5,
  title,
  distance,
  isOpen,
  hours,
  isCurbsidePickupAvailable,
  isSelected,
}: Props) => {
  const renderText = () => {
    const renderHoursText = () => {
      return (
        <Text style={styles.hoursTextStyle}>
          <Text style={{ color: TEXT_GREY }}>Hours: </Text>
          <Text style={{ color: isOpen ? TEXT_GREEN : TEXT_RED }}>
            {isOpen ? 'Open' : 'Closed'}
          </Text>
          <Text> {hours}</Text>
        </Text>
      );
    };

    const renderCurbsideText = () => {
      return (
        <View style={styles.curbsideTextStyle}>
          <Text>Curbside pickup available</Text>
        </View>
      );
    };

    return (
      <View style={styles.textContainerStyle}>
        <Text style={{ fontSize: 18 }}>{title}</Text>
        <Text style={{ fontSize: 14, color: TEXT_GREY }}>
          {distance && `${distance} miles away`}
        </Text>
        {hours && renderHoursText()}
        {isCurbsidePickupAvailable && renderCurbsideText()}
      </View>
    );
  };

  const renderCurbsideIcon = () => {
    // TODO: complete when there're icons available
    return (
      <View>
        <Text>bla</Text>
      </View>
    );
  };

  const renderBottomSection = () => {
    // TODO: complete when there're icons available
    return <View style={{ borderWidth: 1, height: 65 }} />;
  };

  const renderTopSection = () => {
    const renderLeftColumn = () => {
      return (
        <View style={styles.topLeftColumnStyle}>
          {isCurbsidePickupAvailable && renderCurbsideIcon()}
        </View>
      );
    };

    return (
      <View style={styles.topSectionContainerStyle}>
        <View style={{ flexDirection: 'row', flex: 1 }}>
          {renderLeftColumn()}
          {renderText()}
        </View>
      </View>
    );
  };

  const renderCardContent = () => {
    return (
      <View style={{ borderWidth: 1, flex: 1 }}>
        {renderTopSection()}
        {renderBottomSection()}
      </View>
    );
  };

  return (
    <View
      style={[
        { paddingHorizontal },
        isLast ? { width: SCREEN_WIDTH } : { width },
      ]}>
      <View
        style={[
          styles.containerStyle,
          { flex: 1, backgroundColor },
          isLast && { width: width - paddingHorizontal * 2 },
          isSelected && { backgroundColor: '#431' },
        ]}>
        {renderCardContent()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  containerStyle: {
    shadowColor: CARD_SHADOW_COLOR_LIGHT_GREY,
    shadowRadius: 2,
    shadowOffset: { width: 2, height: 4 },
    shadowOpacity: 1,
    elevation: 3,
  },
  topLeftColumnStyle: {
    borderWidth: 1,
    width: 50,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  hoursTextStyle: {
    fontSize: 14,
    marginTop: 15,
  },
  curbsideTextStyle: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  textContainerStyle: {
    flex: 1,
    paddingTop: 15,
    borderWidth: 1,
  },
  topSectionContainerStyle: {
    flex: 1,
    borderWidth: 1,
    paddingBottom: 17,
  },
});

export { CarouselCard };
