import React from 'react';
import {
  View,
  Modal,
  TouchableOpacity,
  SafeAreaView,
  Image,
  StyleSheet,
} from 'react-native';
import { WHITE } from '../../constants/Colors';
import { Assets } from '../../assets/Assets';

interface Props {
  visible: boolean;
  onRequestClose(): void;
  children?: any;
  backgroundColor?: string;
}

const MyModal = ({
  visible,
  children,
  onRequestClose,
  backgroundColor = WHITE,
}: Props) => {
  const renderContent = () => {
    return (
      <View>
        <TouchableOpacity onPress={onRequestClose}>
          <Image source={Assets.CLOSE_BUTTON} style={styles.closeButton} />
        </TouchableOpacity>
        {children}
      </View>
    );
  };

  return (
    <Modal
      animationType="slide"
      transparent
      visible={visible}
      onRequestClose={onRequestClose}>
      <View style={[styles.container, { backgroundColor }]}>
        <SafeAreaView style={{ flex: 1 }}>{renderContent()}</SafeAreaView>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignSelf: 'stretch',
  },
  closeButton: {
    height: 30,
    width: 30,
    marginLeft: 20,
  },
});

export default MyModal;
