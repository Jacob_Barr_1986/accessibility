import { BEER_COLORS } from './Colors';
import { Assets } from '../assets/Assets';
import { Beer } from '../types';

export const minRatingOptionsArr: number[] = [0, 3, 5, 8];
export const minAlcoholOptionsArr: number[] = [0, 1, 2, 3, 4, 5, 6, 7];

export const BEERS: Beer[] = [
  {
    id: 0,
    name: 'Hop Peak IPA',
    color: BEER_COLORS.BEER_COLOR_EBC_26,
    style: 'IPA',
    image: Assets.IPA,
    rating: 10,
    alcohol: 6.5,
    bottleCapacityML: 330,
    description:
      'Simcoe and Citra dry hops form a pinnacle of lush citrus and pine aromas in this modern interpretation of the classic India Pale Ale. Our hopback infuses flavor from whole cone hops to build a complex body, further emboldened by the addition of an oil-rich, concentrated hop flower resin called lupulin powder. This potent powder kicks up a fresh hoppy dominance, which is supported by a backbone of unique specialty malts. Golden amber in color and refreshing in taste, Hop Peak IPA is a congratulatory drink for reaching any of life’s summits.',
  },
  {
    id: 1,
    name: 'Paulaner',
    color: BEER_COLORS.BEER_COLOR_EBC_8,
    style: 'Wheat Beer',
    image: Assets.WHEAT,
    rating: 7,
    alcohol: 5.2,
    bottleCapacityML: 500,
    description:
      'It’s the #1 wheat beer in Germany and one of the world’s favorites. In fact, beer experts call it a masterpiece. In developing this beer, the Paulaner brewmasters have perfected a unique technique with “yeast suspension,” resulting in a uniform, slightly cloudy appearance, consistent quality, and perfect taste.',
  },
  {
    id: 2,
    name: 'London Porter',
    color: BEER_COLORS.BEER_COLOR_EBC_57,
    style: 'Porter',
    image: Assets.PORTER,
    rating: 10,
    alcohol: 5.4,
    bottleCapacityML: 330,
    description:
      "Award winning ale. Rich, dark & complex. The world's finest. Brewed beside the Thames.London Porter takes its name from the Porters who carried goods around the streets of London in the 18th Century. This Prize-Winning beer is brewed using Pale, Crystal, Brown and Chocolate malts combined with Fuggles hops giving a Rich, dark and complex flavour. London Porter boasts wonderful Chocolate notes and a smooth, satisfying finish.",
  },
];
