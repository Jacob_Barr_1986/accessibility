export const TEXT_GREY = '#596067';
export const TEXT_GREEN = '#2D7E24';
export const TEXT_RED = '#C70032';
export const CARD_SHADOW_COLOR_LIGHT_GREY = '#BEC2C9';
export const BLACK = '#000';
export const WHITE = '#FFF';
export const LIGHT_GREY = '#E1E1E1';

export const BEER_COLORS = {
  BEER_COLOR_EBC_1: '#F9F753',
  BEER_COLOR_EBC_6: '#F7F514',
  BEER_COLOR_EBC_8: '#EBE619',
  BEER_COLOR_EBC_12: '#D5BC25',
  BEER_COLOR_EBC_16: '#BE923C',
  BEER_COLOR_EBC_20: '#BF813A',
  BEER_COLOR_EBC_26: '#BC6633',
  BEER_COLOR_EBC_33: '#8C4C31',
  BEER_COLOR_EBC_39: '#5C341A',
  BEER_COLOR_EBC_47: '#261716',
  BEER_COLOR_EBC_57: '#0F0B0A',
  BEER_COLOR_EBC_69: '#080707',
  BEER_COLOR_EBC_79: '#030403',
};
// export const BEER_COLOR_EBC_1 = '#F9F753';
// export const BEER_COLOR_EBC_6 = '#F7F514';
// export const BEER_COLOR_EBC_8 = '#EBE619';
// export const BEER_COLOR_EBC_12 = '#D5BC25';
// export const BEER_COLOR_EBC_16 = '#BE923C';
// export const BEER_COLOR_EBC_20 = '#BF813A';
// export const BEER_COLOR_EBC_26 = '#BC6633';
// export const BEER_COLOR_EBC_33 = '#8C4C31';
// export const BEER_COLOR_EBC_39 = '#5C341A';
// export const BEER_COLOR_EBC_47 = '#261716';
// export const BEER_COLOR_EBC_57 = '#0F0B0A';
// export const BEER_COLOR_EBC_69 = '#080707';
// export const BEER_COLOR_EBC_79 = '#030403';
