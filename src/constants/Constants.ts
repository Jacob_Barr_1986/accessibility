import { Dimensions, Platform } from 'react-native';
import DeviceInfo from 'react-native-device-info';

export const HAS_NOTCH = DeviceInfo.hasNotch();
export const IS_ANDROID: boolean = Platform.OS === 'android';
export const SCREEN_WIDTH: number = Dimensions.get('window').width;
export const SCREEN_HEIGHT: number = Dimensions.get('window').height;

export const DROPBOX_ROW_HEIGHT = 30;
export const BAR_WIDTH = 400;
