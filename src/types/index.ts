import { ImageRequireSource } from 'react-native';

export interface Beer {
  id: number;
  name: string;
  color: string;
  style: string;
  image: ImageRequireSource;
  rating: number;
  description: string;
  alcohol: number;
  bottleCapacityML: number;
}
