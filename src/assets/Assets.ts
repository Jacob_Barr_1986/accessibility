export const Assets = {
  IPA: require('./hop-peak-ipa.png'),
  WHEAT: require('./paulaner.png'),
  PORTER: require('./london-porter.png'),
  FULL_STAR_FILLED: require('./full-star-filled.png'),
  FULL_STAR_UNFILLED: require('./full-star-unfilled.png'),
  HALF_STAR_FILLED: require('./half-star-filled.png'),
  HALF_STAR_UNFILLED: require('./half-star-unfilled.png'),
  CLOSE_BUTTON: require('./close-button.png'),
};
