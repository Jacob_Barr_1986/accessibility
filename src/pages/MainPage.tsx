import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  ListRenderItem,
  SafeAreaView,
  AccessibilityInfo,
  AccessibilityChangeEventHandler,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RootStackParamList } from '../App';
import { BLACK, LIGHT_GREY, WHITE } from '../constants/Colors';
import MyModal from '../components/common/MyModal';
import { Beer } from '../types';
import {
  BEERS,
  minAlcoholOptionsArr,
  minRatingOptionsArr,
} from '../constants/Data';
import BeerPage from '../components/BeerPage';
import BeerCard from '../components/BeerCard';
import { DraggablePickerBar, Dropbox } from '../components/common';

type MainScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Main'>;

interface Props {
  navigation: MainScreenNavigationProp;
}

const MainPage = ({ navigation }: Props) => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [selectedBeerId, setSelectedBeerId] = useState<number>();
  const [minRatingFilter, setMinRatingFilter] = useState<number>(0);
  const [minAlcoholFilter, setMinAlcoholFilter] = useState<number>(0);
  const [screenReaderON, setScreenReaderON] = useState<boolean>(false);
  const selectedBeer = BEERS.find((beer) => beer.id === selectedBeerId);
  const accessibilityModeON = true;

  useEffect(() => {
    const eventHandler: AccessibilityChangeEventHandler = (
      screenReaderEnabled,
    ) => setScreenReaderON(screenReaderEnabled);

    AccessibilityInfo.addEventListener('screenReaderChanged', eventHandler);

    return () => {
      AccessibilityInfo.removeEventListener(
        'screenReaderChanged',
        eventHandler,
      );
    };
  }, []);

  const renderCard: ListRenderItem<Beer> | null = ({ item, index }) => {
    if (item.rating < minRatingFilter || item.alcohol < minAlcoholFilter) {
      return null;
    }
    return (
      <BeerCard
        key={index}
        index={index}
        beerData={item}
        action={() => {
          setSelectedBeerId(item.id);
          setShowModal(true);
        }}
      />
    );
  };

  const renderDescription = () => {
    return (
      <View style={styles.description}>
        <Text style={styles.descriptionText}>
          This app was built specifically for this seminar.
        </Text>
        <Text style={styles.descriptionText}>
          It isn't destined for commercial use but for instruction purposes
          only.
        </Text>
        <Text style={styles.descriptionText}>
          So please don't pay attention to the amateurish design.
        </Text>
        <Text style={styles.descriptionText}>Thanks!</Text>
      </View>
    );
  };

  const renderAlcoholFilter = () => {
    if (screenReaderON) {
      return (
        <Dropbox
          title="Pick Min Alcohol Level"
          optionsArr={minAlcoholOptionsArr}
          action={(alcoholLevel: number) => setMinAlcoholFilter(alcoholLevel)}
        />
      );
    }

    return (
      <DraggablePickerBar
        scalesArr={minAlcoholOptionsArr}
        action={(pickedValue) => {
          setMinAlcoholFilter(pickedValue);
        }}
      />
    );
  };

  return (
    <View style={styles.pageContainer}>
      <SafeAreaView style={styles.container}>
        <Text style={styles.mainTitle}>Accessibility Seminar</Text>
        <Text style={styles.mainSubTitle}>Training App</Text>

        {renderDescription()}

        <Dropbox
          title="Pick Min Beer Rating"
          optionsArr={minRatingOptionsArr}
          action={(rating: number) => setMinRatingFilter(rating)}
        />

        {renderAlcoholFilter()}

        <FlatList
          horizontal
          keyExtractor={(item, index) => index.toString()}
          data={BEERS}
          renderItem={renderCard}
          contentContainerStyle={styles.carouselContentContainer}
          style={styles.carousel}
          showsHorizontalScrollIndicator={false}
        />

        <MyModal
          visible={showModal}
          onRequestClose={() => setShowModal(false)}
          backgroundColor={selectedBeer?.color}>
          <BeerPage selectedBeerInfo={selectedBeer} />
        </MyModal>
      </SafeAreaView>
    </View>
  );
};

const styles = StyleSheet.create({
  pageContainer: {
    flex: 1,
    backgroundColor: '#bcf',
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainTitle: {
    fontSize: 35,
  },
  mainSubTitle: {
    fontSize: 20,
  },
  carousel: {
    marginTop: 60,
    overflow: 'visible',
  },
  carouselContentContainer: {
    height: 200,
    marginLeft: 20,
    alignContent: 'flex-start',
  },
  description: {
    marginVertical: 20,
  },
  descriptionText: {
    fontSize: 15,
    lineHeight: 20,
  },
  beerCardContainer: {
    justifyContent: 'space-between',
    width: 150,
    padding: 10,
    borderRadius: 20,
    shadowColor: BLACK,
    shadowRadius: 10,
    backgroundColor: WHITE,
    shadowOpacity: 0.3,
    marginRight: 20,
  },
  beerBottleImageContainer: {
    height: 120,
    width: 60,
    alignSelf: 'flex-end',
    marginTop: -50,
    position: 'absolute',
  },
  beerName: {
    fontWeight: '500',
    fontSize: 18,
    marginBottom: 5,
  },
  beerStyle: {
    color: 'grey',
  },
});

export default MainPage;
