import { AccessibilityInfo, findNodeHandle } from 'react-native';
import { MutableRefObject } from 'react';

export const setAccessibilityFocus = (
  ref: MutableRefObject<any>,
  timeout = 500,
) => {
  const reactTag = findNodeHandle(ref.current);

  setTimeout(() => {
    reactTag && AccessibilityInfo.setAccessibilityFocus(reactTag);
  }, timeout);
};
